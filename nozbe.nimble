# Package

version       = "0.1.0"
author        = "reisub0"
description   = "A nozbe client"
license       = "MIT"
srcDir        = "src"
bin           = @["nozbe"]

# Dependencies

requires "nim >= 0.18.0"
