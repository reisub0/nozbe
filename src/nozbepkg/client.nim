import HttpClient, asyncdispatch, json
import times, strformat

import osproc,os
import rdstdin, terminal
import streams

type
  Task* = object
    name*    : string
    dueDate* : DateTime
    next*    : bool

proc `$`(task: Task): string =
    # "{" & &""""name":"{$task.name}","dueDate":"{format(inZone(fromUnix(task.dueDate), local()), "dd MMM")}""" & "\"}"
    result = ""
    result = result & task.name
    result = result & ","
    result = result & format(inZone(task.dueDate, local()), "dd MMM")
    result = result & ","
    if task.next:
        result = result & "★"
    else:
        result = result & "☆"
        # &"""{$task.name},{format(inZone(fromUnix(task.dueDate), local()), "dd MMM")}"""

proc login*() =
    let username=readLineFromStdin("Enter Nozbe username: ")
    let password=readPasswordFromStdin("Enter Nozbe password: ")
    echo "Please login somehow"

const Host = "https://api.nozbe.com:3000"
if not os.fileExists("/home/g/.local/.nozbe_token"):
    login()

let AuthFile = open("/home/g/.local/.nozbe_token")
var AuthToken : string
# The first line contains auth token
for line in Authfile.lines:
    AuthToken = line
    break
let client = newAsyncHttpClient()
client.headers=newHttpHeaders({"Authorization": AuthToken})

proc delayedStringGet(path: string, body: string) : Future[string] {.async.} =
    var response = await client.request(url=(Host & path), body=body)
    return await response.body

proc delayedTasksGet() : Future[seq[Task]] {.async.} =
    result = @[]
    let jsonString = await delayedStringGet(path="/list", body="type=task")
    let parsed = parseJson(jsonString)
    echo $parsed
    # assert parsed.kind == JArray
    for task in parsed:
        result.add(
            Task(name: task["name"].getStr(),
                dueDate: parse(layout="yyyy-MM-dd HH:mm:ss", value=task["datetime"].getStr()),
                next: task["next"].getBool()
                )
            )
proc printTasks*() =
    let TaskList = waitFor delayedTasksGet()
    let p = startProcess("column -s , -t", options={poEvalCommand})
    for task in TaskList:
        # if task.next:
        #     continue
        p.inputStream.write($task)
        p.inputStream.write('\n')
    p.inputStream.close()
    echo p.outputStream.readAll()
    discard waitForExit(p)
    p.close()


when isMainModule:
    # discard main()
    printTasks()
